﻿import java.util.Scanner;
import java.lang.Math;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	
       Scanner tastatur = new Scanner(System.in);
      
       //System.out.println("hallo");
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       int nachfrage;
       int anzahltickets;
       

       do {
       //Methode Fahrkarten
       zuZahlenderBetrag = fahrkartenbestellungErfassen();
       
       //Methode-Bezahlung
       eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

       //Fahrkartenausgabe
       fahrkartenAusgeben();
       
       
       //Rückgeldausgabe
       rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
       
       System.out.println("Wollen Sie erneut Fahrkarten bestellen? (ja[1]/nein[2])");
       nachfrage = tastatur.nextInt();
       } while(nachfrage == 1);
       if(nachfrage == 2) {
    	   System.out.println("Auf Wiedersehen.");
       } else if (nachfrage >=3) {
    	   System.out.println("Sie haben keine Wahl getroffen, das Programm bzw. die Bestellung wurde beendet.");
       }
      
    }
    
    public static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	double zuZahlenderBetrag=0.0;
    	System.out.println("Wählen Sie eine Fahrkartenart aus.");
    	System.out.println("Geben Sie die Nummer ein, von der Fahrkarte (Bereich Berlin), die Sie bestellen möchten.");
    	System.out.println("Zur Verfügung stehen: \n "
    			+ "Einzelfahrschein Regeltarif AB [2,90€] (1) \n "
    			+ "Tageskarte Regeltarif AB [8,60€] (2) \n"
    			+ " Kleingruppen-Tageskarte Regeltarif AB [23,50€] (3)");
    	int fahrkartentyp = tastatur.nextInt();
    	System.out.println("Ihre Wahl: " + fahrkartentyp);
    	//double zuZahlenderBetrag = tastatur.nextDouble();
        //int counter=0;
        int counter2=0;
        while(fahrkartentyp > 3 || fahrkartentyp <0) {
        	System.out.println(">>falsche Eingabe<<");
        	System.out.println("Geben Sie eine gültige Fahrkartentypnummer ein. (1, 2, 3)");
        	fahrkartentyp = tastatur.nextInt();
        	System.out.println("Ihre Wahl: " + fahrkartentyp);
        	
        	
        	/*zuZahlenderBetrag = tastatur.nextDouble();
        	counter = counter +1;
        	if(counter == 5) {
        		System.out.println("Aufgrund deiner Fehlversuche wurde dein Ticketpreis auf 1€ gesetzt!");
        		zuZahlenderBetrag=1;
        	}*/
        }
        
        
        switch (fahrkartentyp) {
        case 1:
			zuZahlenderBetrag = zuZahlenderBetrag + 2.90;
			break;
			
        case 2: 
        	zuZahlenderBetrag = zuZahlenderBetrag + 8.60;
        	break;
        	
        case 3: 
        	zuZahlenderBetrag = zuZahlenderBetrag + 23.50;
        	break;
        }
        
        
        System.out.println("Wie viele Tickets wollen Sie? Min.:1, Max.:10");
       
        int AnzahlTickets = tastatur.nextInt();
       /* if(AnzahlTickets > 10 || AnzahlTickets < 1) {
        	System.out.println("Sie haben eine nicht gültige Anzahl eingegeben! \nDie Anzahl wurde auf 1 gesetzt.");
        	AnzahlTickets = 1;
        };*/
        while (AnzahlTickets > 10 || AnzahlTickets < 1) {
        	System.out.println("Sie haben eine nicht gültige Anzahl eingegeben!\nGeben Sie eine gültige Anzahl ein:");
        	AnzahlTickets = tastatur.nextInt();
        	counter2=counter2+1;
        	if(counter2>=5) {
        		System.out.println("Deine Anzahl wurde standardmäßig auf 1 gesetzt.");
        		AnzahlTickets=1;
        	}
        };
        zuZahlenderBetrag = zuZahlenderBetrag*AnzahlTickets;
        System.out.printf("Die Zwischensumme beträgt: %.2f €\n ", zuZahlenderBetrag);
        
        System.out.println("Wollen Sie noch weitere Fahrkarten bestellen? (Ja[1]/Nein[2])");
        int weiter = tastatur.nextInt();
        if(weiter == 1) {
        	do {
        		double wticket = 0;
        		System.out.println("Wählen Sie eine Fahrkartenart aus.");
            	System.out.println("Geben Sie die Nummer ein, von der Fahrkarte (Bereich Berlin), die Sie bestellen möchten.");
            	System.out.println("Zur Verfügung stehen: \n "
            			+ "Einzelfahrschein Regeltarif AB [2,90€] (1) \n "
            			+ "Tageskarte Regeltarif AB [8,60€] (2) \n"
            			+ " Kleingruppen-Tageskarte Regeltarif AB [23,50€] (3)");
            	fahrkartentyp = tastatur.nextInt();
            	System.out.println("Ihre Wahl: " + fahrkartentyp);
        
                counter2=0;
                while(fahrkartentyp > 3 || fahrkartentyp <0) {
                	System.out.println(">>falsche Eingabe<<");
                	System.out.println("Geben Sie eine gültige Fahrkartentypnummer ein. (1, 2, 3)");
                	fahrkartentyp = tastatur.nextInt();
                	System.out.println("Ihre Wahl: " + fahrkartentyp);
                	
                }
                
                
                switch (fahrkartentyp) {
                case 1:
        			wticket = wticket + 2.90;
        			break;
        			
                case 2: 
                	wticket = wticket + 8.60;
                	break;
                	
                case 3: 
                	wticket = wticket + 23.50;
                	break;
                }
                
                
                System.out.println("Wie viele Tickets wollen Sie? Min.:1, Max.:10");
               
                AnzahlTickets = tastatur.nextInt();
                while (AnzahlTickets > 10 || AnzahlTickets < 1) {
                	System.out.println("Sie haben eine nicht gültige Anzahl eingegeben!\nGeben Sie eine gültige Anzahl ein:");
                	AnzahlTickets = tastatur.nextInt();
                	counter2=counter2+1;
                	if(counter2>=5) {
                		System.out.println("Deine Anzahl wurde standardmäßig auf 1 gesetzt.");
                		AnzahlTickets=1;
                	}
                };
                wticket = wticket*AnzahlTickets;
                zuZahlenderBetrag = zuZahlenderBetrag+wticket;
                System.out.printf("Die Zwischensumme beträgt: %.2f €\n ", zuZahlenderBetrag);
                System.out.println("Wollen Sie noch weitere Tickets bestellen? (Ja[1]/Nein[2])");
                weiter = tastatur.nextInt();
        		
        	} while (weiter == 1);
        	
        } 
        if(weiter == 2) {
        	System.out.println("Der Bezahlvorgang wird eingeleitet.");
        } else {
        	System.out.println("Sie haben nicht 1 oder 2 eingegeben. Der Bezahlvorgang wurde auto. eingeleitet.");
        }
    	
        return zuZahlenderBetrag;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag = 0.0;
    	
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen:%.2f €\n" ,(zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   double eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	return eingezahlterGesamtbetrag;
    }

    public static void fahrkartenAusgeben() {
    	System.out.println("\nFahrschein wird ausgegeben");
    	int millisekunde = 250;
    	warte(millisekunde);
        
        System.out.println("\n\n");
    	
    }
    
    public static void warte(int millisekunde) {
    	 
    	//for (int i = 0; i < 8; i++)
    	//{
            //System.out.print("=");
            try {
  			Thread.sleep(millisekunde);
  		} catch (InterruptedException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
         //}
    }
    
    public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
    	
    	
    	double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
         
         {
      	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO. \n",rückgabebetrag);
      	   System.out.println("Wird in folgenden Münzen ausgezahlt:");

             while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
             {
          	  //System.out.println("2 EURO");
          	  int betrag = 2;
          	  String einheit = "EURO";
          	  muenzeAusgeben(betrag, einheit);
          	 
          	  rückgabebetrag=rückgabebetrag-2.0;
             }
             while(rückgabebetrag >= 1.00) // 1 EURO-Münzen
             {
          	  //System.out.println("1 EURO");
          	  int betrag = 1;
        	  String einheit = "EURO";
        	  muenzeAusgeben(betrag, einheit);
          	  rückgabebetrag=rückgabebetrag-1.0;
             }
             while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
             {
          	  //System.out.println("50 CENT");
            	int betrag = 50;
             	String einheit = "CENT";
             	muenzeAusgeben(betrag, einheit);
          	 
          	  rückgabebetrag=rückgabebetrag-0.50;
             }
             while(rückgabebetrag >= 0.20) // 20 CENT-Münzen
             {
          	  //System.out.println("20 CENT");
            	int betrag = 20;
              	String einheit = "CENT";
              	muenzeAusgeben(betrag, einheit); 
          	  rückgabebetrag=Math.round(rückgabebetrag*100.0)/100.0;
          	  rückgabebetrag=rückgabebetrag-0.20;
             }
             while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
             {
          	  //System.out.println("10 CENT");
            	int betrag = 10;
              	String einheit = "CENT";
              	muenzeAusgeben(betrag, einheit);
          	  rückgabebetrag=Math.round(rückgabebetrag*100.0)/100.0;
          	  rückgabebetrag=rückgabebetrag-0.10;
             }
             while(rückgabebetrag >= 0.050)// 5 CENT-Münzen
             {
          	  //System.out.println("5 CENT");
            	int betrag = 5;
              	String einheit = "CENT";
              	muenzeAusgeben(betrag, einheit);
          	  rückgabebetrag=Math.round(rückgabebetrag*100.0)/100.0;
          	  rückgabebetrag=rückgabebetrag-0.050;
             }
         }

         System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                            "vor Fahrtantritt entwerten zu lassen!\n"+
                            "Wir wünschen Ihnen eine gute Fahrt.");
    	
    }
    
    public static void muenzeAusgeben(int betrag, String einheit) {
    	System.out.printf("\n%2d " + einheit, betrag);
    }

}