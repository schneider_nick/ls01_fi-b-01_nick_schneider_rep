import java.util.Random;
import java.util.Scanner;

public class twist {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Bitte geben Sie ein Wort ein, dass getwistet werden soll");
		String word = scan.next();
		scan.close();
		Random rand = new Random();

		int lowrand = 1; // zufallszahl auf ein minimum begrenzen
		int highrand = (word.length()) - 1; // zufallszahl auf ein maximum begrenzen
		int zufallszahl = 0;

		char[] twist = new char[word.length()];

		char[] ch = new char[word.length()];
		for (int i = 0; i < word.length(); i++) {
			ch[i] = word.charAt(i);
			twist[i] = '-';

		}

		twist[0] = ch[0];
		twist[word.length() - 1] = ch[word.length() - 1];
		ch[0] = '-';
		ch[word.length() - 1] = '-';

		int z = 1;
		do {

			zufallszahl = rand.nextInt(highrand - lowrand) + lowrand;

			if (ch[zufallszahl] != '-') {
				twist[z] = ch[zufallszahl];
				ch[zufallszahl] = '-';
				z += 1;
			}

		} while ((new String(twist).contains("-")));

		System.out.print("Das getwistete Wort lautet: ");
		System.out.println(twist);

	}

}
