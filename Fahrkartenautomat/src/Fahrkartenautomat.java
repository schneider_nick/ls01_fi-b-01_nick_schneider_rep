﻿import java.io.IOException;
import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		boolean run = true;
		do {

			double zuZahlenderBetrag;
			double eingezahlterGesamtbetrag;

			zuZahlenderBetrag = fahrkartenbestellungErfassen();
			if (zuZahlenderBetrag == -100) {
				run = false;
			} else {

				// Geldeinwurf
				// -----------
				eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);

				// Fahrscheinausgabe
				// -----------------
				fahrkartenAusgeben();

				// Rückgeldberechnung und -Ausgabe
				// -------------------------------

				rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

				System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
						+ "Wir wünschen Ihnen eine gute Fahrt.");

				warte(4000);
				System.out.println("\n\n\n\n\n\n\n");

				clrscr();
			}
		} while (run == true);
		System.out.println("Wird heruntergefahren");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\nSystem Shutdown");
	}

	public static double fahrkartenbestellungErfassen() {
		Scanner mytastatur = new Scanner(System.in);
		int anzahlTickets;
		double preis = 0;
		double gesamtpreis = 0;
		boolean bruch = false;

		System.out.println("Fahrkartenbestellvorgang:");
		System.out.println("=========================\n");
		do {
			System.out.println("\nWählen Sie ihre Wunschfahrkarte für Berlin aus:");
			System.out.printf("%-5s%8s%38s\n", "Nr.", "| Bezeichnung ", "| Preis in Euro");
			System.out.println("-----|------------------------------------|--------------");
			String[] Karten = { "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC",
					"Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC",
					"Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC",
					"Kleingruppen-Tageskarte Berlin ABC" };
			double[] Preise = { 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90 };
			for (int i = 0; i <= Karten.length - 1; i++) {
				System.out.printf("%1s%2d%-2s%-35s%1s%5.2f%1s\n", "[", i + 1, "] | ", Karten[i], "| ", Preise[i], "€");
			}
			System.out.println("-----|------------------------------------|--------------");
			System.out.printf("%1s%2s%-2s%-35s%1s%5.2f%1s\n", "[", "0", "] | ", "Bezahlen", "| ", gesamtpreis, "€");

			System.out.print("\nIhre Wahl: ");
			int auswahl = mytastatur.nextInt();
			if (auswahl == 0) {
				bruch = true;
			} else if (auswahl == -12345) {
				// geheimer deaktivierungscode
				gesamtpreis = -100;
				System.out.println("Sie haben den geheimen Deaktivierungscode einegegeben.");
				return gesamtpreis;

			} else if (auswahl > Karten.length || auswahl < 0) {
				System.out.println("Fehlerhafte eingabe");
			} else {
				preis = Preise[auswahl - 1];
				do {
					System.out.print("\nAnzahl der Tickets:");
					anzahlTickets = mytastatur.nextInt();

					if (anzahlTickets > 10 || anzahlTickets < 1) {
						System.out.println(
								"Der Anzahl liegt außerhalb des Bereiches von 1 bis 10. Bitte versuchen Sie es erneut.");

					}
				} while (anzahlTickets < 1 || anzahlTickets > 10);
				gesamtpreis = (anzahlTickets * preis) + gesamtpreis;

			}
		} while (bruch == false);

		return gesamtpreis;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner mytastatur = new Scanner(System.in);
		double eingeworfeneMünze;
		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 20 Euro): ");
			eingeworfeneMünze = mytastatur.nextDouble();
			if (eingeworfeneMünze > 20) {
				System.out.println("\n" + eingeworfeneMünze + " Euro werden wieder Ausgegeben.");
				System.out.println("Bitte maximal 20 Euro einwerfen.");
				eingeworfeneMünze = 0;
			}
			eingezahlterGesamtbetrag += eingeworfeneMünze;

		}
		return eingezahlterGesamtbetrag;

	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");
	}

	public static void warte(int millisekunden) {
		try {
			Thread.sleep(millisekunden);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rückgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f %s \n", rückgabebetrag, " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				muenzeAusgeben(2, "EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				muenzeAusgeben(1, "EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				muenzeAusgeben(50, "CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				muenzeAusgeben(20, "CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				muenzeAusgeben(10, "CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				muenzeAusgeben(5, "CENT");
				rückgabebetrag -= 0.05;
			}
		}
	}

	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + " " + einheit);

	}

	public static void clrscr(){

	    //Clears Screen in java

	    try {

	        if (System.getProperty("os.name").contains("Windows"))

	            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();

	        else

	            Runtime.getRuntime().exec("clear");

	    } catch (IOException | InterruptedException ex) {}

	}
}