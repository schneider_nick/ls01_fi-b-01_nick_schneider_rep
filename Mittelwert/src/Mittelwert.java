import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double x = 2.0;
      double y = 4.0;
      double m;
      
      Scanner ms = new Scanner(System.in);
      
      System.out.println("Bitte geben Sie die erste Zahl ein: ");
      x = ms.nextDouble();
      
      System.out.println("Bitte geben Sie die zweite Zahl ein: ");
      y = ms.nextDouble();
      
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
     
      m = berechneMittelwert(x, y);
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      mittelwertAusgabe(x, y, m);
      ms.close();
   }
   
   public static double berechneMittelwert(double zahl1, double zahl2) {
	   double mittel;
	   mittel = (zahl1 + zahl2) / 2.0;
	   return mittel;
   }
   
   public static void mittelwertAusgabe(double x, double y, double m) {
	   System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }
}
