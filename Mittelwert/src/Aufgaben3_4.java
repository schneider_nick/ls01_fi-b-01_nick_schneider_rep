import java.util.Scanner;

public class Aufgaben3_4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner myScan =new Scanner(System.in);
		System.out.println("geben sie den ersten Wiederstand ein: ");
		double r1 = myScan.nextDouble();
		System.out.println("geben sie den zweiten Wiederstand ein: ");
		double r2 = myScan.nextDouble();
		double reihe = reihenschaltung(r1, r2);
		double parallel = parallelschaltung(r1, r2);
		System.out.println("In Reihe: "+ reihe + " Parallel: "+ parallel);

	}
	
	public static double reihenschaltung(double r1, double r2) {
		double reihe = r1+r2;
		return reihe;
	}
	
	public static double parallelschaltung(double r1, double r2) {
		double parallel = 1/((1/r1)+(1/r2));
		return parallel;
	}

}
