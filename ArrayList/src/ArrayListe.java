import java.util.ArrayList;
import java.util.Scanner;

public class ArrayListe {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Integer> list = new ArrayList<Integer>();

		for (int i = 0; i < 20; i++) {
			int r = (int) (Math.random() * 9 + 1);
			list.add(i, r);

		}
		System.out.println("Ausgabe: Liste mit 20 Zufallszahlen");
		for (int i = 0; i < 20; i++) {
			System.out.println("Arraylist [" + (i) + "] : " + list.get(i));

		}
		System.out.print("\nEine Zahl zwischen 1 und 9: ");
		Scanner scan = new Scanner(System.in);
		int suchzahl = scan.nextInt();

		int counter = 0;
		for (int i = 0; i < 20; i++) {
			if (suchzahl == list.get(i)) {
				counter++;
			}
		}
		System.out.println("\n\nDie Zahl " + suchzahl + " kommt " + counter + " mal in der Liste vor.");
		System.out.println("\nDie Zahl kommt an folgenden Indices in der Liste vor:");
		for (int i = 0; i < 20; i++) {
			if (suchzahl == list.get(i)) {
				System.out.println("Arraylist [" + (i) + "] : " + list.get(i));
			}

		}
		System.out.println("\nListe nach L�schung von " + suchzahl + ":");
		for (int i = 0; i < list.size(); i++) {
			if (suchzahl == list.get(i)) {
				list.remove(i);
				i--;

			}

		}
		for (int i = 0; i < list.size(); i++) {
			System.out.println("Arraylist [" + (i) + "] : " + list.get(i));

		}
		System.out.println("\nListe nach Einfuegen von 0 hinter jeder 5");
		for (int i = 0; i < list.size(); i++) {
			if (5 == list.get(i)) {
				list.add(i + 1, 0);

			}
		}
		for (int i = 0; i < list.size(); i++) {
			System.out.println("Arraylist [" + (i) + "] : " + list.get(i));

		}
	}
}
