
public class Summe {

	public static void main(String[] args) {
		int summe = 0, zahl = 1;

		while (zahl <= 50) {
			summe = summe + zahl;
			zahl++;
		}

		System.out.println(zahl);
		System.out.println(summe);

		summe = 0;
		zahl = 1;

		do {
			summe = summe + zahl;
			zahl++;
		} while (zahl <= 50);
		System.out.println("\n" + zahl);
		System.out.println(summe);

		summe = 0;
		zahl = 1;

		for (int i = 1; i <= 50; i++) {
			summe = summe + i;
		}
		// System.out.println("\n" + i); Nicht zul�ssig da i nur in der Schleife
		// existiert.
		System.out.println("\n" + summe);

	}

}
