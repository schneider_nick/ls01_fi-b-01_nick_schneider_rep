import java.util.Scanner;

public class Addition {

	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;
		

        //1.Programmhinweis
        Programmhinweis();

        //4.Eingabe
       
        zahl1 = Eingabe(" 1. Zahl:");
       
        zahl2 = Eingabe(" 2. Zahl:");

        //3.Verarbeitung
        erg = Verarbeitung(zahl1, zahl2);

        //2.Ausgabe
        ausgabe(erg, zahl1, zahl2);
        
	}
	
	public static void Programmhinweis() {
		
		System.out.println("Hinweis: ");
        System.out.println("Das Programm addiert 2 eingegebene Zahlen. ");
	}
	
	public static double Eingabe(String text) {
		Scanner sc = new Scanner(System.in);
		System.out.println(text);
		double zahl = sc.nextDouble();
		return zahl;
	}
	
	public static double Verarbeitung(double zahl1, double zahl2) {
		
		double erg = zahl1 + zahl2;
		return erg;
		
	}
	
	public static void ausgabe(double erg, double zahl1, double zahl2) {
		System.out.println("Ergebnis der Addition");
        System.out.printf("%.2f = %.2f+%.2f", erg, zahl1, zahl2);
	}

}
