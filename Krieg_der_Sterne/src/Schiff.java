
/**
 * @author Schneider
 * @version 1.0
 * 
 */
import java.util.ArrayList;

public class Schiff {
	/**
	 * @param name                  ist der name des Schiffes.
	 * @param energiversorgung      gibt die verbleibende Energie in prozent an.
	 * @param schilde               gibt die verbleibende Schildleistung in prozent
	 *                              an.
	 * @param lebenserhaltung       gibt den zustand der lebenserhaltungssysteme in
	 *                              prozent an.
	 * @param h�lle                 gibt die verbleibende Strukturelle Integrit�t
	 *                              der H�lle in prozent an.
	 * @param photonentorpedos      gibt die anzahl an verbleibenden
	 *                              photonentorpedos an.
	 * @param reperaturandroiden    gibt die anzahl der verbleibenden
	 *                              Reperaturroboter an.
	 * @param broadcastKommunikator speichert alle �bertragungen auf dem schiff.
	 * @param ladungsliste          gibt alle Ladungen an die sich auf dem Schiff
	 *                              befinden.
	 * 
	 */
	private String name;
	private int energieversorgung;
	private int schilde;
	private int lebenserhaltung;
	private int h�lle;
	private int photonentorpedos;
	private int reperaturandroiden;
	public static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsliste = new ArrayList<Ladung>();

	// Konstruktoren:
	/**
	 * Hier wird die Klasse Schiff ohne parameter konstruiert.
	 */
	public Schiff() {

	}

	/**
	 * 
	 * @param name               ist der name des Schiffes.
	 * @param energieversorgung  gibt die verbleibende Energie in prozent an.
	 * @param schilde            gibt die verbleibende Schildleistung in prozent an.
	 * @param lebenserhaltung    gibt den zustand der lebenserhaltungssysteme in
	 *                           prozent an.
	 * @param h�lle              gibt die verbleibende Strukturelle Integrit�t der
	 *                           H�lle in prozent an.
	 * @param photonentorpedos   gibt die anzahl an verbleibenden photonentorpedos
	 *                           an.
	 * @param reperaturandroiden gibt die anzahl der verbleibenden Reperaturroboter
	 *                           an.
	 */
	public Schiff(String name, int energieversorgung, int schilde, int lebenserhaltung, int h�lle, int photonentorpedos,
			int reperaturandroiden) {
		this.name = name;
		this.energieversorgung = energieversorgung;
		this.schilde = schilde;
		this.lebenserhaltung = lebenserhaltung;
		this.h�lle = h�lle;
		this.photonentorpedos = photonentorpedos;
		this.reperaturandroiden = reperaturandroiden;

	}

//Verwaltungsmethoden:
	/**
	 * In dieser methode kann der Name ausgelesen werden.
	 * 
	 * @return name des Schiffes als String.
	 */
	public String getName() {
		return name;
	}

	/**
	 * In dieser Methode kann der name gesetzt werden.
	 * 
	 * @param name ist der name des Schiffes.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * In dieser methode kann der Wert der Energiversorgung ausgelesen werden.
	 * 
	 * @return energiversorgung als Integer.
	 */
	public int getEnergieversorgung() {
		return energieversorgung;
	}

	/**
	 * In dieser Methode kann der Wert der Enerieversorgung gesetzt werden.
	 * 
	 * @param energieversorgung gibt die verbleibende Energie in prozent an.
	 */
	public void setEnergieversorgung(int energieversorgung) {
		this.energieversorgung = energieversorgung;
	}

	/**
	 * In dieser methode kann der Wert der Schilde ausgelesen werden.
	 * 
	 * @return schilde als Integer.
	 */
	public int getSchilde() {
		return schilde;
	}

	/**
	 * In dieser Methode kann der Wert der Schilde gesetzt werden.
	 * 
	 * @param schilde gibt die verbleibende Schildleistung in prozent an.
	 */
	public void setSchilde(int schilde) {
		this.schilde = schilde;
	}

	/**
	 * In dieser methode kann der Wert der Lebenserhaltung ausgelesen werden.
	 * 
	 * @return lebenserhaltung als Integer.
	 */
	public int getLebenserhaltung() {
		return lebenserhaltung;
	}

	/**
	 * In dieser Methode kann der Wert der Lebenserhaltung gesetzt werden.
	 * 
	 * @param lebenserhaltung gibt den zustand der lebenserhaltungssysteme in
	 *                        prozent an.
	 */
	public void setLebenserhaltung(int lebenserhaltung) {
		this.lebenserhaltung = lebenserhaltung;
	}

	/**
	 * In dieser methode kann der Wert der Strukturelle Integrit�t der H�lle
	 * ausgelesen werden.
	 * 
	 * @return h�lle als Integer.
	 */
	public int getH�lle() {
		return h�lle;
	}

	/**
	 * In dieser Methode kann der Wert der Strukturelle Integrit�t der H�lle gesetzt
	 * werden.
	 * 
	 * @param h�lle gibt die verbleibende Strukturelle Integrit�t der H�lle in
	 *              prozent an.
	 */
	public void setH�lle(int h�lle) {
		this.h�lle = h�lle;
	}

	/**
	 * In dieser methode kann Anzahl der Photonentorpedos ausgelesen werden.
	 * 
	 * @return photonentorpedos als Integer.
	 */
	public int getPhotonentorpedos() {
		return photonentorpedos;
	}

	/**
	 * In dieser Methode kann die Anzahl der Photonentorpedos gesetzt werden.
	 * 
	 * @param photonentorpedos gibt die anzahl an verbleibenden photonentorpedos an.
	 */
	public void setPhotonentorpedos(int photonentorpedos) {
		this.photonentorpedos = photonentorpedos;
	}

	/**
	 * In dieser Methode kann Anzahl der Reperaturandroiden ausgelesen werden.
	 * 
	 * @return reperaturandroiden Anzahl als Integer.
	 */
	public int getReperaturandroiden() {
		return reperaturandroiden;
	}

	/**
	 * In dieser Methode kann die Anzahl der Reperaturandroiden gesetzt werden.
	 * 
	 * @param reperaturandroiden gibt die anzahl der verbleibenden Reperaturroboter
	 *                           an.
	 */
	public void setReperaturandroiden(int reperaturandroiden) {
		this.reperaturandroiden = reperaturandroiden;
	}

//Methoden

	/**
	 * In dieser Methode kann Ladung zu dem Schiff hinzu gef�gt werden.
	 * 
	 * @param Ladung besteht aus Typ der Ladung und Anzahl des Types.
	 */
	public void addLadung(Ladung Ladung) {
		ladungsliste.add(Ladung);
	}

	/**
	 * In dieser Methode kann der Zustand des Schifffes mit all seinen Atributen
	 * ausgegeben werden.
	 */
	public void zustandRaumschiff() {
		System.out.println("\nZustand des Schiffes " + name + ": \nName: " + name + "\nEnergiversorgung: "
				+ energieversorgung + "\nSchilde: " + schilde + "\nLebenserhaltung: " + lebenserhaltung + "\nH�lle: "
				+ h�lle + "\nPhotonentorpedos: " + photonentorpedos + "\nReperaturandroiden: " + reperaturandroiden);
	}

	/**
	 * In dieser Methode kann das gesamte Ladungsverzeichnis mit Typ und Anzahl
	 * ausgegeben werden.
	 */
	public void ladungsverzeichnisAusgeben() {
		System.out.println("\nLadungsverzeichniss der " + name + ": ");
		for (Ladung i : ladungsliste) {
			System.out.println(i.getTyp() + ": " + i.getMenge() + "x");
		}
	}

	/**
	 * Mit dieser Methode k�nnen Photonentorpedos auf ein anderes Schiff geschossen
	 * werden. Bei jedem Schuss wird ein Torpedo aus dem SChiff abgerechnet und eine
	 * Nachricht an alle gesendet. Wenn keine Torpedos mehr vorhanden sind, dann
	 * macht es blo� "Click"
	 * 
	 * @param zielSchiff gibt das Schiff an, auf welches geschossen wird.
	 */
	public void photonentorpedoSchie�en(Schiff zielSchiff) {
		if (photonentorpedos > 0) {
			photonentorpedos--;
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			treffer(zielSchiff);

		} else {
			nachrichtAnAlle("-=*Click*=-");
		}
	}

	/**
	 * Mit dieser Methode k�nnen die Phasenkanonen auf ein anderes Schiff schie�en.
	 * Bei jedem Schuss wird die Energiversorgung um 50% vermindertund eine
	 * Nachricht an alle gesendet. Wenn nicht mehr genug Energie vorhanden ist, dann
	 * macht es blo� "Click"
	 * 
	 * @param zielSchiff gibt das Schiff an, auf welches geschossen wird.
	 */
	public void phasenkanonenSchie�en(Schiff zielSchiff) {
		if (energieversorgung >= 50) {
			energieversorgung -= 50;
			nachrichtAnAlle("Phasenkanonen abgeschossen");
			treffer(zielSchiff);

		} else {
			nachrichtAnAlle("-=*Click*=-");
		}
	}

	/**
	 * In dieser Methode wird nach einem treffer der Schaden am Schiff berechnet und
	 * eine Nachricht �ber den treffer an alle gesendet. Ein treffer zieht 50% der
	 * Schilde ab. Wenn die Schilde auf 0% sinken wird der H�lle und der
	 * Energiversorgung 50% abgezogen. Wenn die H�lle auf 0 % sinkt wird die
	 * Lebenserhaltung auch auf 0% gesekt und damit zerst�rt. Passiert dies, wird
	 * eine Nachricht an alle �bermittelt, dass die Lebenserhaltung ausf�llt.
	 * 
	 * @param zielSchiff
	 */
	private void treffer(Schiff zielSchiff) {
		System.out.println(zielSchiff.name + " wurde getroffen!");
		zielSchiff.setSchilde(zielSchiff.getSchilde() - 50);
		if (zielSchiff.getSchilde() < 0) {
			zielSchiff.setSchilde(0);
		}
		if (zielSchiff.getSchilde() <= 0) {
			zielSchiff.setH�lle(zielSchiff.getH�lle() - 50);
			if (zielSchiff.getH�lle() < 0) {
				zielSchiff.setH�lle(0);
			}
			zielSchiff.setEnergieversorgung(zielSchiff.getEnergieversorgung() - 50);
			if (zielSchiff.getEnergieversorgung() < 0) {
				zielSchiff.setEnergieversorgung(0);
			}
		}
		if (zielSchiff.getH�lle() <= 0) {
			zielSchiff.setLebenserhaltung(0);
			nachrichtAnAlle(zielSchiff.getName() + ": Lebenserhaltung ausgefallen! \nKritischer Systemfehler!");
		}
	}

	/**
	 * Hiermit wird die Nachricht an alle auf der Konsole ausgegeben und auch dem
	 * BroadcastKommunikator hinzugef�gt.
	 * 
	 * @param message ist der String der die Nachricht enth�lt.
	 */
	public void nachrichtAnAlle(String message) {
		System.out.println(message);
		broadcastKommunikator.add(message);
	}

	/**
	 * Hier kann der Inhalt des BroadcastKommunikators zur�ckgegeben werden .
	 * 
	 * @return broadcastKommunikator als ArrayList String.
	 */
	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return broadcastKommunikator;

	}

}
