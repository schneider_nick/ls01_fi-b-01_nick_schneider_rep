/**
 * @author Schneider
 * @version 1.0
 */
public class Ladung {
	/**
	 * @param typ ist der Typ der Ladung, also die Bezeichnung.
	 * @param menge gibt die anzahl des einen Typs an.
	 */
	private String typ;
	private int menge;

	//Konstruktoren
	/**
	 * Hier wird die Klasse Ladung ohne parameter konstruiert.
	 */
	public Ladung() {
		
	}
	
	/**
	 * Hier wird die Klasse Ladung mit allen parametern konstruiert.
	 * @param typ ist der Typ der Ladung, also die Bezeichnung.
	 * @param menge gibt die anzahl des einen Typs an.
	 */
	public Ladung(String typ, int menge) {
		this.typ = typ;
		this.menge = menge;
	}
	
	//verwaltungsmethoden
	
	/**
	 * In dieser Methoden kann der typ ausgelesen werden.
	 * @return typ als String
	 */
	public String getTyp() {
		return typ;
	}
	
	/**
	 * In dieser Methoden kann der typ gesetzt werden.
	 * @param typ ist der Typ der Ladung, also die Bezeichnung.
	 */
	public void setTyp(String typ) {
		this.typ = typ;
	}
	
	/**
	 * In dieser Methoden kann menge ausgelesen werden.
	 * @return menge als int
	 */
	public int getMenge() {
		return menge;
	}
	
	/**
	 * In dieser Methoden kann die Menge gesetzt werden.
	 * @param menge gibt die anzahl des einen Typs an.
	 */
	public void setMenge(int menge) {
		this.menge = menge;
	}
	
	
}
