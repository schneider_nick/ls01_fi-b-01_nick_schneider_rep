/**
 * 
 * @author Schneider
 *
 */
public class Main {

	public static void main(String[] args) {
		Ladung ladung1 = new Ladung();
		Ladung ladung2 = new Ladung();
		Ladung ladung3 = new Ladung();
		Ladung ladung4 = new Ladung();
		Ladung ladung5 = new Ladung();
		Ladung ladung6 = new Ladung();
		Ladung ladung7 = new Ladung();

		Schiff klingonen = new Schiff();
		Schiff romulaner = new Schiff();
		Schiff vulkanier = new Schiff();

		ladung1.setTyp("Ferengi Schneckensaft");
		ladung1.setMenge(200);
		ladung2.setTyp("Bat'leth Klingonen Schwert");
		ladung2.setMenge(200);
		ladung3.setTyp("Borg-Schrott");
		ladung3.setMenge(5);
		ladung4.setTyp("Rote Materie");
		ladung4.setMenge(2);
		ladung5.setTyp("Plasma-Waffe");
		ladung5.setMenge(50);
		ladung6.setTyp("Forschungssonde");
		ladung6.setMenge(35);
		ladung7.setTyp("Photonentorpedos");
		ladung7.setMenge(3);

		klingonen.setPhotonentorpedos(1);
		klingonen.setEnergieversorgung(100);
		klingonen.setSchilde(100);
		klingonen.setHülle(100);
		klingonen.setLebenserhaltung(100);
		klingonen.setName("IKS Hegh'th");
		klingonen.setReperaturandroiden(2);

		romulaner.setPhotonentorpedos(2);
		romulaner.setEnergieversorgung(100);
		romulaner.setSchilde(100);
		romulaner.setHülle(100);
		romulaner.setLebenserhaltung(100);
		romulaner.setName("IRW Khazara");
		romulaner.setReperaturandroiden(2);

		vulkanier.setPhotonentorpedos(0);
		vulkanier.setEnergieversorgung(80);
		vulkanier.setSchilde(80);
		vulkanier.setHülle(50);
		vulkanier.setLebenserhaltung(100);
		vulkanier.setName("Ni'Var");
		vulkanier.setReperaturandroiden(5);

		klingonen.addLadung(ladung1);
		klingonen.addLadung(ladung2);
		romulaner.addLadung(ladung3);
		romulaner.addLadung(ladung4);
		romulaner.addLadung(ladung5);
		vulkanier.addLadung(ladung6);
		vulkanier.addLadung(ladung7);
		// _____________________________________________________________________

		klingonen.photonentorpedoSchießen(romulaner);
		romulaner.phasenkanonenSchießen(klingonen);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		klingonen.zustandRaumschiff();
		klingonen.photonentorpedoSchießen(romulaner);
		klingonen.photonentorpedoSchießen(romulaner);
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		romulaner.zustandRaumschiff();
		romulaner.ladungsverzeichnisAusgeben();
		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgeben();

	}

}
