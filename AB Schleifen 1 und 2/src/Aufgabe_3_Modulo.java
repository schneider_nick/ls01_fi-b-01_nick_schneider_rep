
public class Aufgabe_3_Modulo {

	public static void main(String[] args) {

		System.out.println("Zahlen die durch 7 teilbar sind: ");
		for (int i = 1; i <= 200; i++) {
			if (i % 7 == 0) {
				System.out.print(i + " ");

			}
		}
		System.out.println(" ");
		System.out.println("\n" + "Zahlen die durch 4 aber nicht durch 5 teilbar sind: ");
		for (int i = 1; i <= 200; i++) {
			if (i % 5 != 0 && i % 4 == 0) {
				System.out.print(i + " ");

			}

		}
	}
}
